// ==UserScript==
// @name         Fenci
// @namespace    http://tampermonkey.net/
// @version      0.7
// @description  try to take over the world!
// @match        http://mp-dochazka-a/GateHouse/*
// @match        http://mp-dochazka-a/gatehouse/*
// @require      https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.min.js
// @grant        none
// ==/UserScript==

rand = function(m, n){return Math.floor((Math.random() * n) + m);};
var zlovolniFenci =
        [
            {prijmeni: "Fenek", jmeno: "Berberský", organizace: "Africká sahara"},
            {prijmeni: "Kobra", jmeno: "Nubijská", organizace: "Etiopie"},
            {jmeno: "Tlustorepý", prijmeni: "Štír", organizace: "Australská divočina"},
            {jmeno: "Čtyřhranka", prijmeni: "Medůza", organizace: "Pacifický oceán"},
            {prijmeni: "Paropucha", jmeno: "Krkovitá", organizace: "ČR"},
            {jmeno: "Lysý", prijmeni: "Rypoš", organizace: "Východní Afrika"},
            {prijmeni: "Chřestivec", jmeno: "Protáhlý", organizace: "Pobřeží kostariky"},
            {jmeno: "Ostnitý", prijmeni: " Moloch", organizace: "Australské vody"},
            {jmeno: "Celebeská", prijmeni: "Babirusa", organizace: "Srí Lanka"},
            {jmeno: "Beznosý", prijmeni: "Kočkodan", organizace: "Afrika"},
            {jmeno: "Yetti", prijmeni: "Krab", organizace: "Velikonoční ostrovy"},
            {jmeno: "Mořská", prijmeni: "Mihule", organizace: "Oceánie"},
            {jmeno: "Tatarská", prijmeni: "Sajga", organizace: "Euroásie"},
            {jmeno: "Paví", prijmeni: "Strašek", organizace: "ČR"},
            {prijmeni: "Břehouš", jmeno: "Černoocasý", organizace: "ČR"},
            {prijmeni: "Kuňka", jmeno: "Žlutobřichá", organizace: "ČR"},
            {prijmeni: "Jasoň", jmeno: "Dymnivkový", organizace: "ČR"},
            {prijmeni: "Brunduk", jmeno: "Páskovaný", organizace: "Severní Amerika"},
            {prijmeni: "Dvojzoborožec", jmeno: "Žlutozubý", organizace: "Střední Afrika"},
            {prijmeni: "Kudlanka", jmeno: "Nábožná", organizace: "Subtropy a Tropy"},
            {prijmeni: "Mýval", jmeno: "Severní", organizace: "Spojené Státy, Kanada a Evropa"},
            {prijmeni: "Morčák", jmeno: "Velký", organizace: "Severní Evropa"},
            {prijmeni: "Ondatra", jmeno: "Pižmová", organizace: "Severní Amerika,Evropa a Asie"},
            {prijmeni: "Pásovec", jmeno: "Devítipásý", organizace: "Severní Amerika"},
            {prijmeni: "Varan", jmeno: "Komodský", organizace: "Indonésie"}
        ];
window.addFenek = (function(){
    var shuffleArray = function(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    };
    var addLeadingZero=function(n){return ('0'  + n).slice(-2);};
    var i=0;
    var randomAnimals = shuffleArray(zlovolniFenci);//zlovolniFenci[rand(0, zlovolniFenci.length -1)];
    return function(){
        i++;
        var randomAnimal = randomAnimals[i];
        var string;
        if(i < randomAnimals.length){
            string = '<tr class="fenci"><td><a href="http://www.google.com/search?q=';
            string += randomAnimal.prijmeni+'+'+randomAnimal.jmeno;
            string += '" target="_blank"><img src="../images/Prop.gif" style="" /></a> <a href="javascript:addFenek()"><img src="../images/Delete.gif"></a></td><td>';
            string += addLeadingZero(rand(0,23))+':'+addLeadingZero(rand(0,59))+':00<td><b>'+randomAnimal.prijmeni+'</b><td>'+randomAnimal.jmeno+'<td>'+randomAnimal.organizace+'</td>';
            $("#T_LIST").prepend(string);
        }else i=0;
        //$(".fenci").css("display", "none");
    };
    //var pocetOsob = Number($("#ctl00_ContentPlaceHolder1_LBL_PocOsob").text());
    //$("#ctl00_ContentPlaceHolder1_LBL_PocOsob").text(++pocetOsob);
})();
$(document).ready(function(){
    addFenek();
    //console.log("localStorage.fenci: "+localStorage.fenci);
    if(localStorage.fenci === undefined) localStorage.fenci = "";
    else if(localStorage.fenci == "checked") $(".fenci").show();
    else $(".fenci").hide();
    //console.log("localStorage.fenci: "+localStorage.fenci);
    $("#ctl00_ContentPlaceHolder1_CHB_Aktualni").parent().prepend("<input type='checkbox' "+ localStorage.fenci +" name='fenek' id='fenek' style='font-size: 60%'>");
    $('#fenek').click(function(){
        if(!$('#fenek').is(':checked')){
            $(".fenci").hide();
            localStorage.fenci = "";
        }
        else{
            $(".fenci").show();
            localStorage.fenci = "checked";
        }
        //console.log("localStorage.fenci: "+localStorage.fenci);
    });
});
