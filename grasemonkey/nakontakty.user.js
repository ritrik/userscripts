// ==UserScript==
// @name         Na kontakty
// @namespace    http://tampermonkey.net/
// @version      0.1.2
// @description  try to take over the world!
// @author       You
// @match        http://mp-dochazka-a/GateHouse/GHReview.aspx
// @match        http://mp-dochazka-a/gateHouse/ghreview.aspx
// @match        http://mp-dochazka-a/gatehouse/GHReview.aspx
// @match        http://s-mpa-dochazka1/gatehouse/ghreview.aspx
// @match        http://s-mpa-dochazka1/GateHouse/GHReview.aspx
// @match        http://s-mpa-dochazka1.ad.mpsv.cz/gatehouse/ghreview.aspx
// @match        http://s-mpa-dochazka1.ad.mpsv.cz/GateHouse/GHReview.aspx
// @require      https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.min.js
// @grant        none
// ==/UserScript==
//https://sps.mpsv.cz/sites/kb/Lists/Contacts/AllItems.aspx?InplaceSearchQuery=
//#T_LIST > tbody > tr > td:nth-child(1) > a:nth-child(1)
//#T_LIST > tbody > tr > td:nth-child(3)

$(document).ready(function(){
	$("#T_LIST > tbody > tr").each(function(){
		//console.log($(this).children("td:nth-child(1)").children("a").first().attr("href"));
		var jmeno = encodeURIComponent($(this).children("td:nth-child(3)").text() +" "+ $(this).children("td:nth-child(4)").text());
		//console.log(jmeno);
		$(this).children("td:nth-child(1)").children("a").first().attr("href", "https://sps.mpsv.cz/sites/kb/Lists/Contacts/AllItems.aspx?InplaceSearchQuery=" + jmeno);
	});
});
