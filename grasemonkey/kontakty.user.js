// ==UserScript==
// @name         Kontakty
// @namespace    https://intra.mpsv.cz/
// @version      0.9.2
// @description  Upravuje seznam kontaktů, do titulku stránky přepíše jméno zobrazené osoby pro větší přehlednost. Plus nějáká kouzla :-P.
// @match        https://intra.mpsv.cz/kontakty*
// @require      https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.min.js
// @grant        none
// ==/UserScript==

//body > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td > div:nth-child(1) > font > b
//var table = document.getElementsByTagName("table")[5].rows[1].tables[0].tables[0].rows[0].get
//var jmeno = document.getElementsByTagName("b")[2].innerHTML;
function addFavicon(src){
    $('head').append('<link href="' + src + '" rel="shortcut icon" type="image/x-icon">');
}
$(document).ready(function () {

addFavicon("https://sps.mpsv.cz/_layouts/images/favicon.ico");
$("title").html("Kontakt");
var cesta = $("body > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr > td > table > tbody > tr:nth-child(1) > td > div:nth-child(1) > font > b");
var jmeno = $.trim(cesta.html());
if(!$.isEmptyObject(jmeno)) $("title").html(jmeno);

if(jmeno.match("\\bPech\\b")){
    cesta
         .prepend("&spades; ")
         .append(" &spades;");
}

});
